﻿namespace KiemTraCaNhan.Models
{
    public class Reports
    {
        public int ID { get; set; }
        public int AccountID { get; set; }
        public int LogsID { get; set; }
        public int TransactionID { get; set; }
        public string? Reportname { get; set; }
        public DateTime? Reportdate { get; set; }

    }
}
