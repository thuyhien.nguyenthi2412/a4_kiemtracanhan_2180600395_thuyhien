﻿namespace KiemTraCaNhan.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string? FirstName { get; set; }

        public string? LastName { get; set; }
        public string? ContactandAdress { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
    }
}
