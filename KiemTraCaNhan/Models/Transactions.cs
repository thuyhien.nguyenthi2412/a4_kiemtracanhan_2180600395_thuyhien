﻿namespace KiemTraCaNhan.Models
{
    public class Transactions
    {
        public int ID { get; set; }
        public int EmployeesID { get; set; }
        public int CustomerID { get; set; }
        public string? Name { get; set; }
    }
}
